package com.yu.mapper;

import com.yu.pojo.Subject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程科目 Mapper 接口
 * </p>
 *
 * @author 陈浩南
 * @since 2023-06-20
 */
public interface SubjectMapper extends BaseMapper<Subject> {

}
