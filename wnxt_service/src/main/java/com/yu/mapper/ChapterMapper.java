package com.yu.mapper;

import com.yu.DTO.ChapterVO;
import com.yu.pojo.Chapter;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * 课程 Mapper 接口
 * </p>
 *
 * @author 陈浩南
 * @since 2023-06-24
 */
public interface ChapterMapper extends BaseMapper<Chapter> {

        @Select("select id, title from wnxt_chapter where course_id = #{courseId}")
        @ResultType(ChapterVO.class)
        List<ChapterVO> listChapterVOByCourseId(String courseId);
}
