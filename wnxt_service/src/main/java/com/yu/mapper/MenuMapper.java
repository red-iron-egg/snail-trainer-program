package com.yu.mapper;

import com.yu.pojo.Menu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 陈浩南
 * @since 2023-06-21
 */
public interface MenuMapper extends BaseMapper<Menu> {

}
