package com.yu.mapper;

import com.yu.pojo.Trainer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 培训师 Mapper 接口
 * </p>
 *
 * @author 陈浩南
 * @since 2023-06-13
 */
public interface TrainerMapper extends BaseMapper<Trainer> {

}
