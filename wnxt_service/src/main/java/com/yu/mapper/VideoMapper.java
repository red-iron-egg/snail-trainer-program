package com.yu.mapper;

import com.yu.DTO.VideoVO;
import com.yu.pojo.Video;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * 课程视频 Mapper 接口
 * </p>
 *
 * @author 陈浩南
 * @since 2023-06-24
 */
public interface VideoMapper extends BaseMapper<Video> {

    @Select("select id, title from wnxt_video where chapter_id = #{chapterId}")
    @ResultType(VideoVO.class)
    List<VideoVO> listVideoVOByChapterId(String chapterId);
}
