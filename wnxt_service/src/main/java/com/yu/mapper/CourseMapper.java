package com.yu.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yu.DTO.CoursePublishVo;
import com.yu.DTO.CourseVO;
import com.yu.pojo.Course;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程 Mapper 接口
 * </p>
 *
 * @author 陈浩南
 * @since 2023-06-20
 */
public interface CourseMapper extends BaseMapper<Course> {

     CoursePublishVo selectPublish(String id);

    IPage<CourseVO> findAllPageCourse(IPage<Course> ip);

}
