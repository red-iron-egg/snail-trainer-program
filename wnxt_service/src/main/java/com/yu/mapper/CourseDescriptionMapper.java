package com.yu.mapper;

import com.yu.pojo.CourseDescription;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程简介 Mapper 接口
 * </p>
 *
 * @author 陈浩南
 * @since 2023-06-20
 */
public interface CourseDescriptionMapper extends BaseMapper<CourseDescription> {

}
