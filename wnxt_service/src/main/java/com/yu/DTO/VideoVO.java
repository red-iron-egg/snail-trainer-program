package com.yu.DTO;


import lombok.Data;

@Data
public class VideoVO {
    /*视频ID*/
    private String id;
    /*表示视频名称*/
    private String title;
}
