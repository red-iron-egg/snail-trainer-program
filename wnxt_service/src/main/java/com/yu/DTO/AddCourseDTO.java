package com.yu.DTO;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class AddCourseDTO {
    /*课程ID*/
    private String id;
    /*课程标题*/
    private String title;
    /*一级分类ID*/
    private String subjectParentId;
    /*二级分类ID*/
    private String subjectId;
    /*培训师ID*/
    private String trainerId;
    /*总课时*/
    private Integer lessonNum;
    /*课程描述*/
    private String description;
    /*课程封面*/
    private String cover;
    /*课程价格*/
    private BigDecimal price;
    @ApiModelProperty("报名数量")
    private long buyCount;


}