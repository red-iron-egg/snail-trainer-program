package com.yu.DTO;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@Data
@ApiModel
public class HotCourseVO {
    @ApiModelProperty("课程id")
    private String id;
    @ApiModelProperty("标题")
    private String title;
    @ApiModelProperty("图片")
    private String cover;
    @ApiModelProperty("价格")
    private String price;
    @ApiModelProperty("课时")
    private Integer lessonNum;
    @ApiModelProperty("观看人数")
    private long buyCount;



}
