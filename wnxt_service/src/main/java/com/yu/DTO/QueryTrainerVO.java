package com.yu.DTO;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.Date;

@Data
@ApiModel(value = "培训师搜索条件业务对象")
public class QueryTrainerVO {
    @ApiModelProperty("培训师姓名")
    private String name;
    @ApiModelProperty("培训师头衔")
    private Integer level;
    @ApiModelProperty("入驻的起始时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date createTimeStart;
    @ApiModelProperty("入驻的结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date createTimeEnd;
}
