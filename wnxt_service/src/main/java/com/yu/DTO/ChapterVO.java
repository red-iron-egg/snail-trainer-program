package com.yu.DTO;


import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ChapterVO {
    /*章节ID*/
    private String id;
    /*章节名称*/
    private String title;
    /*章节下面的视频*/
    private List<VideoVO> videoVOList = new ArrayList<>();
}
