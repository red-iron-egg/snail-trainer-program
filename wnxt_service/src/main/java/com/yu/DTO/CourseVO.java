package com.yu.DTO;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class CourseVO {


    @ApiModelProperty("id")
    private String id;
    @ApiModelProperty("图片")
    private String cover;
    @ApiModelProperty("标题")
    private String title;
    @ApiModelProperty("价格")
    private String price;
    @ApiModelProperty("购买数")
    private long buyCount;
    @ApiModelProperty("视频数")
    private Integer videoCount;


}
