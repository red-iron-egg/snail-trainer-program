package com.yu.DTO;

import lombok.Data;

@Data
public class CoursePublishVo {
    private String id;
    private String title;
    private String cover;
    private Integer lessonNum;
    private String subjectOne;
    private String subjectTwo;
    private String trainerName;
    private String price;//只用于显示
}