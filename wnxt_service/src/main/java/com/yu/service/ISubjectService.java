package com.yu.service;

import com.yu.pojo.Subject;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 课程科目 服务类
 * </p>
 *
 * @author 陈浩南
 * @since 2023-06-20
 */
public interface ISubjectService extends IService<Subject> {

    List<Subject> listByParentId(String id);
}

