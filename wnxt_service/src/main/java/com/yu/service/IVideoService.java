package com.yu.service;

import com.yu.pojo.Video;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程视频 服务类
 * </p>
 *
 * @author 陈浩南
 * @since 2023-06-24
 */
public interface IVideoService extends IService<Video> {

}
