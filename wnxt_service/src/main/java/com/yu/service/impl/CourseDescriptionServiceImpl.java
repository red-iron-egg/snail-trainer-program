package com.yu.service.impl;

import com.yu.pojo.CourseDescription;
import com.yu.mapper.CourseDescriptionMapper;
import com.yu.service.ICourseDescriptionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 课程简介 服务实现类
 * </p>
 *
 * @author 陈浩南
 * @since 2023-06-20
 */
@Service
public class CourseDescriptionServiceImpl extends ServiceImpl<CourseDescriptionMapper, CourseDescription> implements ICourseDescriptionService {

}
