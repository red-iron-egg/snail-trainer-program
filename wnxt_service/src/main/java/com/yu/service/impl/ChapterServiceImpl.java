package com.yu.service.impl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import cn.hutool.core.bean.BeanUtil;
import com.yu.DTO.ChapterVO;
import com.yu.DTO.VideoVO;
import com.yu.mapper.VideoMapper;
import com.yu.pojo.Chapter;
import com.yu.mapper.ChapterMapper;
import com.yu.pojo.Video;
import com.yu.service.IChapterService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yu.service.IVideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author 陈浩南
 * @since 2023-06-24
 */
@Service
public class ChapterServiceImpl extends ServiceImpl<ChapterMapper, Chapter> implements IChapterService {

    @Autowired
    private ChapterMapper chapterMapper;
    @Autowired
    private VideoMapper videoMapper;
    @Autowired
    private IVideoService videoService;
    @Override
    public List<ChapterVO> listChapterAndVideoById(String courseId) {
        //lambda 的查询条件
        LambdaQueryWrapper<Chapter> wrapper = Wrappers.lambdaQuery(Chapter.class).eq(Chapter::getCourseId, courseId);

        //查询章节
        List<Chapter> list = list(wrapper);

        //进行复制
        List<ChapterVO> chapterVOList = BeanUtil.copyToList(list, ChapterVO.class);

        for (ChapterVO chapterVO : chapterVOList) {

            //通过章节的id 查询  视频
            String chapterId = chapterVO.getId();
            LambdaQueryWrapper<Video> qr = Wrappers.lambdaQuery(Video.class).eq(Video::getChapterId, chapterId);
            List<Video> videos = videoService.list(qr);
            //需要吧视频转成 vo
            List<VideoVO> videoVOList = BeanUtil.copyToList(videos, VideoVO.class);
            chapterVO.setVideoVOList(videoVOList);

        }

        //stream 的操作
//        List<ChapterVO> chapterVOList = list.stream().map(chapter -> {
//            ChapterVO chapterVO = new ChapterVO();
//            return chapterVO;
//        }).collect(Collectors.toList());

        return chapterVOList;
}
    @Override
    public boolean deleteById(String chapterId) {
        // 1. 判断该章节下面是否有关联的视频
        List<VideoVO> videoVOList = videoMapper.listVideoVOByChapterId(chapterId);
        // 2. 如果没有这直接删除
        if(videoVOList == null || videoVOList.size() == 0){
            return removeById(chapterId);
        }
        // 3. 如果有全删除
        videoMapper.deleteBatchIds(videoVOList);
        return removeById(chapterId);
    }
}

