package com.yu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yu.DTO.QueryTrainerVO;
import com.yu.config.PageData;
import com.yu.pojo.Trainer;
import com.yu.mapper.TrainerMapper;
import com.yu.service.TrainerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 培训师 服务实现类
 * </p>
 *
 * @author 陈浩南
 * @since 2023-06-13
 */
@Service
public class TrainerServiceImpl extends ServiceImpl<TrainerMapper, Trainer> implements TrainerService {
   @Autowired
   private TrainerMapper tm;
    @Override
    public PageData listSearch(Integer currentPage, Integer pageSize, QueryTrainerVO queryTrainerDTO) {
        QueryWrapper<Trainer> wrapper = new QueryWrapper<>();
        if(queryTrainerDTO != null){
            if(!StringUtils.isEmpty(queryTrainerDTO.getName())){
                wrapper.like("name", queryTrainerDTO.getName());
            }
            if(queryTrainerDTO.getLevel() != null){
                wrapper.eq("level", queryTrainerDTO.getLevel());
            }
            if(queryTrainerDTO.getCreateTimeStart() != null){
                wrapper.ge("create_time", queryTrainerDTO.getCreateTimeStart());
            }
            if(queryTrainerDTO.getCreateTimeEnd() != null){
                wrapper.le("create_time", queryTrainerDTO.getCreateTimeEnd());
            }
        }
        // 构建分页对象
        IPage<Trainer> page = new Page<>(currentPage, pageSize);
        // 查询
        IPage<Trainer> trainerIPage = tm.selectPage(page, wrapper);
        // 返回数据
        return new PageData(trainerIPage.getTotal(), trainerIPage.getRecords());
    }
}
