package com.yu.service.impl;

import cn.hutool.core.bean.BeanUtil ;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yu.DTO.AddCourseDTO;
import com.yu.DTO.CoursePublishVo;
import com.yu.DTO.CourseVO;
import com.yu.DTO.HotCourseVO;
import com.yu.config.PageData;
import com.yu.mapper.CourseDescriptionMapper;
import com.yu.mapper.CourseMapper;
import com.yu.pojo.Chapter;
import com.yu.pojo.Course;
import com.yu.pojo.CourseDescription;
import com.yu.pojo.Video;
import com.yu.service.IChapterService;
import com.yu.service.ICourseService;
import com.yu.service.IVideoService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author 陈浩南
 * @since 2023-06-20
 */
@Service
public class CourseServiceImpl extends ServiceImpl<CourseMapper, Course> implements ICourseService {

    @Autowired
    private CourseMapper cm;
    @Autowired
    private CourseDescriptionMapper cdm;
    @Autowired
    private IVideoService iv;
    @Autowired
    private IChapterService ics;


    @Override
    @Transactional
    public String addCourse(AddCourseDTO addCourseDTO) {
        Course course = new Course();
        BeanUtils.copyProperties(addCourseDTO, course);
        if(addCourseDTO.getId() == null){
            // 新增操作
            cm.insert(course);
        }else{
            // 修改操作
            cm.updateById(course);
        }
        CourseDescription description = new CourseDescription();
        description.setId(course.getId());
        description.setDescription(addCourseDTO.getDescription());
        System.out.println(addCourseDTO.getId());
        if(addCourseDTO.getId() == null){
            // 新增操作
            cdm.insert(description);
        }else{
            // 修改操作
            cdm.updateById(description);
        }
        return course.getId();
    }

    @Override
    @Transactional
    public AddCourseDTO getDtoById(String courseId) {
        Course course1 = cm.selectById(courseId);
        AddCourseDTO ad = new AddCourseDTO();
        BeanUtils.copyProperties(course1, ad);
        CourseDescription description = cdm.selectById(courseId);
        System.out.println(description);
        ad.setDescription(description.getDescription());
        System.out.println("ad = " + ad);
        return ad;
    }

    @Override
    public CoursePublishVo getPublish(String id) {
        return cm.selectPublish(id);
    }

    @Override
    public boolean Publish(String id) {
        LambdaUpdateWrapper<Course> lambdaUpdate = Wrappers.lambdaUpdate(Course.class);
        lambdaUpdate.eq(Course::getId,id);
        lambdaUpdate.set(Course::getStatus,"Normal");
        return update(lambdaUpdate);
    }

    @Override
    public PageData pageAll(Integer currentPage, Integer pageSize) {
        IPage<Course> ip=new Page<Course>(currentPage,pageSize);
        IPage<Course> courseIPage = cm.selectPage(ip, null);
        return new PageData(courseIPage.getTotal(),courseIPage.getRecords());
    }

    @Override
    public List<HotCourseVO> newAll() {
        IPage<Course> ip=new Page<Course>(1,4);
        LambdaQueryWrapper<Course> cw = Wrappers.lambdaQuery(Course.class);
        cw.eq(Course::getStatus,"Normal");
        cw.orderByDesc(Course::getUpdateTime);
        IPage<Course> courseIPage = cm.selectPage(ip, cw);
        List<HotCourseVO> hotCourseVOS = BeanUtil.copyToList(courseIPage.getRecords(), HotCourseVO.class);
        return hotCourseVOS;
    }

    @Override
    public PageData findAllPageCourse(Integer currentPage, Integer pageSize) {
        IPage<Course> ip=new Page<>();
        IPage<CourseVO> cv =cm.findAllPageCourse(ip);
        return new PageData(cv.getTotal(),cv.getRecords());
    }

    @Override
    public Boolean deltAll(String id) {
        LambdaQueryWrapper<Video> qv=Wrappers.lambdaQuery(Video.class);
        qv.eq(Video::getCourseId,id);
        LambdaQueryWrapper<Chapter> lc=Wrappers.lambdaQuery(Chapter.class);
        lc.eq(Chapter::getCourseId,id);
        if (iv.list(qv)!=null){
            iv.remove(qv);
        }
        if (ics.list(lc)!=null){
            ics.remove(lc);
        }
        return true;
    }
    @Override
    public List<HotCourseVO> hotAll() {
        IPage<Course> ip=new Page<Course>(1,4);
        LambdaQueryWrapper<Course> cw = Wrappers.lambdaQuery(Course.class);
        cw.eq(Course::getStatus,"Normal");
        cw.orderByDesc(Course::getViewCount);
        IPage<Course> courseIPage = cm.selectPage(ip, cw);
        List<HotCourseVO> hotCourseVOS = BeanUtil.copyToList(courseIPage.getRecords(), HotCourseVO.class);
        return hotCourseVOS;
    }
}
