package com.yu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yu.mapper.SubjectMapper;
import com.yu.pojo.Subject;
import com.yu.service.ISubjectService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 课程科目 服务实现类
 * </p>
 *
 * @author 陈浩南
 * @since 2023-06-20
 */
@Service
public class SubjectServiceImpl extends ServiceImpl<SubjectMapper, Subject> implements ISubjectService {



    @Override
    public List<Subject> listByParentId(String id) {
        LambdaQueryWrapper<Subject> sjLQW= Wrappers.lambdaQuery(Subject.class);
        sjLQW.eq(Subject::getParentId,id);

        return list(sjLQW);
    }
}
