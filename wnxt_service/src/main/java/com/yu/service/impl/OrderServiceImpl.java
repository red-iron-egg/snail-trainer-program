package com.yu.service.impl;

import cn.hutool.core.util.RandomUtil;
import com.yu.pojo.Course;
import com.yu.pojo.Order;
import com.yu.mapper.OrderMapper;
import com.yu.service.ICourseService;
import com.yu.service.IOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 陈浩南
 * @since 2023-06-28
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements IOrderService {
    @Autowired
    private ICourseService is;

    @Override
    public Order add(String courseId) {
        Course course = is.getById(courseId);
        Order od=new Order();
        od.setCourseId(courseId);
        od.setUserId(RandomUtil.randomLong(1000000L));
        od.setPrice(course.getPrice());
        od.setStatus(1);
        od.setMobile("136739582362");

        return od;
    }
}
