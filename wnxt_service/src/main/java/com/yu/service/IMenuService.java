package com.yu.service;

import com.yu.pojo.Menu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 陈浩南
 * @since 2023-06-21
 */
public interface IMenuService extends IService<Menu> {

}
