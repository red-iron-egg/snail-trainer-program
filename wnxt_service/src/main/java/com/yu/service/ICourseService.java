package com.yu.service;

import com.yu.DTO.AddCourseDTO;
import com.yu.DTO.CoursePublishVo;
import com.yu.DTO.HotCourseVO;
import com.yu.config.PageData;
import com.yu.pojo.Course;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author 陈浩南
 * @since 2023-06-20
 */
public interface ICourseService extends IService<Course> {

    String  addCourse(AddCourseDTO addCourseDTO);

    AddCourseDTO getDtoById(String courseId);

    CoursePublishVo getPublish(String id);

    boolean Publish(String id);

    PageData pageAll(Integer currentPage, Integer pageSize);

    List<HotCourseVO> newAll();

    PageData findAllPageCourse(Integer currentPage, Integer pageSize);

    Boolean deltAll(String id);

    List<HotCourseVO> hotAll();
}



