package com.yu.service;

import com.yu.DTO.ChapterVO;
import com.yu.pojo.Chapter;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author 陈浩南
 * @since 2023-06-24
 */
public interface IChapterService extends IService<Chapter> {

    List<ChapterVO> listChapterAndVideoById(String courseId);

    boolean deleteById(String id);

 }
