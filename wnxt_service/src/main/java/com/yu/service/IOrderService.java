package com.yu.service;

import com.yu.pojo.Order;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 陈浩南
 * @since 2023-06-28
 */
public interface IOrderService extends IService<Order> {

    Order add(String courseId);
}
