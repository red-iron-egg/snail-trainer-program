package com.yu.service;

import com.yu.DTO.QueryTrainerVO;
import com.yu.config.PageData;
import com.yu.pojo.Trainer;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 培训师 服务类
 * </p>
 *
 * @author 陈浩南
 * @since 2023-06-13
 */
public interface TrainerService extends IService<Trainer> {
    public PageData listSearch(Integer currentPage, Integer pageSize, QueryTrainerVO queryTrainerDTO);
}
