package com.yu.service;

import com.yu.pojo.CourseDescription;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程简介 服务类
 * </p>
 *
 * @author 陈浩南
 * @since 2023-06-20
 */
public interface ICourseDescriptionService extends IService<CourseDescription> {

}
