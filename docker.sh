#!/bin/bash
# 容器名称
container_name="wnxt"
# 镜像名称
image_name="wnxt"

# 判断容器是否存在
if docker ps -a --format "{{.Names}}" | grep -q "$container_name"; then
  echo "容器 $container_name 存在"

  # 关闭容器
  echo "关闭容器 $container_name"
  docker stop $container_name

  # 删除容器
  echo "删除容器 $container_name"
  docker rm $container_name
else
  echo "容器 $container_name 不存在"
fi

# 删除镜像
if docker images --format "{{.Repository}}" | grep -q "$image_name"; then
  echo "镜像 $image_name 存在"
  echo "删除镜像 $image_name"
  docker rmi $image_name
else
  echo "镜像 $image_name 不存在"
fi

# 重新构建镜像
echo "重新构建镜像 $image_name"
docker build -t $image_name .

# 启动容器
echo "启动容器 $container_name"
docker run -id --name $container_name -p 8083:8083 $image_name