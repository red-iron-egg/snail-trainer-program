package com.yu.task;


import com.yu.util.Contents;
import com.yu.util.MinioClientUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
@Slf4j
public class myRedis {
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private MinioClientUtil minioClientUtil;

    @Scheduled(cron = "0/10 * * * * ?")
    public void deleteTask() throws Exception {
        log.info("定时");
        clearImage();
    }

    public void clearImage() throws Exception {
        Set set = redisTemplate.opsForSet().difference(Contents.ALL_IMAGE, Contents.DB_IMAGE);
        for (Object o : set) {
            minioClientUtil.removeObject(o.toString());
        }
    }


}
