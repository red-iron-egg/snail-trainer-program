package com.yu.util;
public class Contents {

    /**
     * 存储所有的图片
     */
    public static final String ALL_IMAGE = "wn:image:all";

    /**
     * 存储数据库中的图片
     */
    public static final String DB_IMAGE = "wn:image:db";


}