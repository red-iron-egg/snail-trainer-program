package com.yu.controller;
import com.yu.config.HttpStatus;
import com.yu.config.Result;


public class BaseController {
    /**
     * 返回成功的响应结果
     * @return
     */
    public Result success(){
        return new Result(HttpStatus.SUCCESS, "操作成功");
    }
    /**
     * 返回失败的响应结果
     * @return
     */
    public Result error(){
        return new Result(HttpStatus.ERROR, "操作失败");
    }
    /**
     * 判断操作影响的记录数，返回对应的结果
     * @param row
     * @return
     */
    public Result toResult(Integer row){
        return row > 0 ? success() : error();
    }
    /**
     * 判断操作返回的状态，返回对应的结果
     * @param flag
     * @return
     */
    public Result toResult(Boolean flag){
        return flag ? success() : error();
    }
    /**
     * 返回带有查询数据的响应结果
     * @param data
     * @return
     */
    public Result toDataResult(Object data){
        return new Result(HttpStatus.SUCCESS, "查询数据成功", data);
    }
}
