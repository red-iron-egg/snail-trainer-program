package com.yu.controller;

import com.yu.DTO.AddCourseDTO;
import com.yu.DTO.CoursePublishVo;
import com.yu.DTO.HotCourseVO;
import com.yu.config.PageData;
import com.yu.config.Result;
import com.yu.service.ICourseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author 陈浩南
 * @since 2023-06-20
 */
@RestController
@RequestMapping("/course")
@Api(tags = "课程控制器")
@CrossOrigin
public class CourseController extends BaseController {

    @Autowired
    private ICourseService iCourseService;

    @PostMapping
    @ApiOperation("添加课程方法")
    public Result addCourse(@RequestBody AddCourseDTO addCourseDTO) {
        System.out.println("addCourseDTO.getId() = " + addCourseDTO.getId());
        String id = iCourseService.addCourse(addCourseDTO);
        return toDataResult(id);
    }


    @ApiOperation("id查课程")
    @GetMapping("/{courseId}")
    public Result getById(@PathVariable String courseId) {
        AddCourseDTO addCourseDTO = iCourseService.getDtoById(courseId);
        return toDataResult(addCourseDTO);
    }

    @ApiOperation("id查展示课程")
    @GetMapping("/publish/{id}")
    public Result getPublish(@PathVariable String id) {
        CoursePublishVo addCourseDTO = iCourseService.getPublish(id);
        return toDataResult(addCourseDTO);
    }

    @ApiOperation("id发布课程")
    @PutMapping("/publish/{id}")
    public Result Publish(@PathVariable String id) {
        boolean addCourseDTO = iCourseService.Publish(id);
        return toResult(addCourseDTO);
    }


    @ApiOperation("分页展示数据")
    @PostMapping("/{currentPage}/{pageSize}")
    public Result pageAll(
            @PathVariable Integer currentPage,
            @PathVariable Integer pageSize) {
        PageData pd = iCourseService.pageAll(currentPage, pageSize);
        return toDataResult(pd);
    }

    @ApiOperation("展示最新数据")
    @PostMapping("/new")
    public Result getNew() {
        List<HotCourseVO> pd = iCourseService.newAll();
        return toDataResult(pd);
    }
    @ApiOperation("展示最新数据")
    @PostMapping("/hot")
    public Result getHot() {
        List<HotCourseVO> pd = iCourseService.hotAll();
        return toDataResult(pd);
    }


    @ApiOperation("展示前四数据")
    @GetMapping("/{currentPage}/{pageSize}")
    public Result queryAll(
            @PathVariable Integer currentPage,
            @PathVariable Integer pageSize) {
        PageData pd = iCourseService.findAllPageCourse(currentPage, pageSize);
        return toDataResult(pd);
    }

    @ApiOperation("课程删除")
    @DeleteMapping("/{id}")
    public Result deltAll(@PathVariable String id) {
        iCourseService.removeById(id);
        return toResult(iCourseService.deltAll(id));
    }


}
