package com.yu.controller;

import com.yu.DTO.ChapterVO;
import com.yu.config.Result;
import com.yu.pojo.Chapter;
import com.yu.service.IChapterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author 陈浩南
 * @since 2023-06-24
 */
@Api(tags = "章节管理")
@RestController
@RequestMapping("/ct")
public class ChapterController extends BaseController{
    @Autowired
    private IChapterService iChapterService;

    @ApiOperation("根据课程ID查询章节及其视频")
    @GetMapping("/video/{courseId}")
    public Result listChapterAndVideoByCourseId(@PathVariable String courseId){
        List<ChapterVO> chapterVOList =  iChapterService.listChapterAndVideoById(courseId);
        return toDataResult(chapterVOList);
    }
    @ApiOperation("新增或者修改")
    @PostMapping
    public Result saveOrUpdate(@RequestBody Chapter chapter){
        boolean flag = iChapterService.saveOrUpdate(chapter);
        return toResult(flag);
    }
    @ApiOperation("删除章节")
    @DeleteMapping("/{id}")
    public Result delete(@PathVariable String id){
        boolean flag = iChapterService.deleteById(id);
        return toResult(flag);
    }
    @ApiOperation("根据ID查询")
    @GetMapping("/{id}")
    public Result getById(@PathVariable String id){
        Chapter chapter = iChapterService.getById(id);
        return toDataResult(chapter);
    }


}





