package com.yu.controller;

import com.yu.config.Result;
import com.yu.pojo.Course;
import com.yu.pojo.Order;
import com.yu.service.ICourseService;
import com.yu.service.IOrderService;
import com.yu.service.impl.OrderServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.lang.annotation.Target;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 陈浩南
 * @since 2023-06-28
 */
@RestController
@RequestMapping("/order")
@Api(tags = "订单控制器")
public class OrderController extends BaseController{

    @Autowired
    private IOrderService os;
    @Autowired
    private ICourseService is;


    @ApiOperation("添加订单")
    @PostMapping("/{courseId}")
    public Result addOrder(@PathVariable String courseId){
       Order od= os.add(courseId);
       os.save(od);
        return toDataResult(od);
    }
    @ApiOperation("查询课程")
    @GetMapping("/{orderId}")
    public Result getCourse(@PathVariable String orderId){
//        Course byId = is.getById(courseId);
        Order od = os.getById(orderId);
        return toDataResult(od);
    }





}
