package com.yu.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 课程简介 前端控制器
 * </p>
 *
 * @author 陈浩南
 * @since 2023-06-20
 */
@RestController
@RequestMapping("/courseDescription")
public class CourseDescriptionController {

}
