package com.yu.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yu.config.PageData;
import com.yu.config.Result;
import com.yu.pojo.Menu;
import com.yu.service.IMenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 陈浩南
 * @since 2023-06-21
 */
@RestController
@RequestMapping("/menu")
@Api(tags = "菜单控制器")
public class MenuController extends BaseController {
///menu/all/{pageNum}/{pageSize}


    @Autowired
    private IMenuService iMenuService;

    @GetMapping("/all/{pageNum}/{pageSize}")
    @ApiOperation("全菜单分页查询")
    public Result getAll(@PathVariable Integer pageNum,@PathVariable Integer pageSize){
        Page<Menu> menuPage = new Page<>(pageNum, pageSize);
        Page<Menu> page = iMenuService.page(menuPage);
        return toDataResult(new PageData(page.getTotal(),page.getRecords()));
    }
    @GetMapping("/one/{id}")
    @ApiOperation("全菜单分页查询")
    public Result getOne(@PathVariable String id){
        return toDataResult(iMenuService.getById(id));
    }
    @PostMapping
    public Result add(@RequestBody Menu menu){
        iMenuService.saveOrUpdate(menu);
        return toResult(true);
    }
    @DeleteMapping("/{id}")
    @ApiOperation("id删除")
    public Result deltById(@PathVariable String id){
        boolean b = iMenuService.removeById(id);
        return toResult(b);
    }


}
