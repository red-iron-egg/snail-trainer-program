package com.yu.controller;


import cn.hutool.json.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.alipay.api.response.AlipayTradePagePayResponse;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yu.config.Result;
import com.yu.pojo.Course;
import com.yu.pojo.Order;
import com.yu.service.ICourseService;
import com.yu.service.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Controller
        @RequestMapping("/pay")
public class PayController extends BaseController {
    @Autowired
    private IOrderService is;
    @Autowired
    private ICourseService ics;


    //沙箱账号: wqjnvl4429@sandbox.com
//密码:    111111
//支付宝网关（固定）
    public static final String saveURL = "https://openapi-sandbox.dl.alipaydev.com/gateway.do";
    //APPID 即创建应用后生成。
    public static final String appId = "9021000122695724";
    //私钥
    public static final String privateKey = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCSrkuCaNSd3K3LQFVx2JUKXqHB/YdAsD4yKfxjAVqqTt07wye/aPDa1SMuLVhcG5SQGm3w1hjXlmsoKSezR70Lb7DyBH8RkMDnpsUdk8BdVvKMXdcgYJAyucX8SJ0XrA/TQe61VNZR4hj1cZJGv0VGX0CW9ZhFUeK7A/a6Bfj6iaOVzv/xrRs+c/5c14iZefwdFbKqwHXHFdc1ocryloxxwsfcNwFXPlcch8vPONpywF+YSHUi0WN+af5XwLB4+JDLGqGQKUE81+5boI8uZZ0IO53W9m/1GxNIUglypX+5yMWi4F7aX68+26IcSunbmLE3m6EwMiTsxSAYEtU1mrU1AgMBAAECggEAWvOCVLml5MDabuZUVQRj3gsxp/UJsb3WwwovukAztNiz64OmGNT8S+TPLKnnWS2s3/HTa/uFaeCWT0+9kzgPa5D1GgxZsk2cn8dUAHQGWpo4SXtCkovtvdZdgoKQDgadxXPjjUKuphVZPpAOkJm3XdxgU3qz0X/l4+qgSVa8vLQ7Lv+E9P3gNeTCHb17Fn12sguOxAH03l298+0aMl7gD73KQDh2mAaKhlZtnWJLcuCUXEyDOVnHLeDEpGz06SktwpThTFYdtCBrs9tcENwA2ioU5dBRHIwi0UvQa81SAjf6CwQspbp0BaEObT0Rpv3hXGZfKeycQRWeTFEy5YQXnQKBgQDfBfjUg8VZV95kWOYpbq8u3S0hFotQgYZ6uZ/Px7ZAn9/adeG3xiRzgk1VQrqqWzzcNXUa1H2WFgqY1ygtXfydVS94oV0SMcwFOARXg9247Cn1r5uN6PRE/SkqwcGYmmn0Z0i844tGe8DOsttC4bx8nGNJPlyIw6o2vGBuFBc5wwKBgQCoXo5cIpzT6bcsK8wSi1tCLLypPkaDu7z00aoC+37ukw+JEJCZmeNpjAXcb0aM10aWaW2vbZYaZ49w3ANNWXn2xpgzQBRoyNLS+AJ/3yUyIU3uiM2p4WGq7+RpvwWlJILRfbmXDcKJ4qP2kYlpjcRXEFUxSBwj24gdMtpao/RtpwKBgAzHP9GVVzpeQXzIZy+aiukJfSt3KjZLpZPkjNaP385mGqJevLcV4ELKBXpMH3vhTGRln7cQ071EuAi30zJ+PxoYpmaoADgpqEHoSr9k69BLn7/W9vezkENfqfssLNZ8inOZc153PySjtJFjSBLV8kHFiB7LNuwTIe6O7ouAFTCXAoGBAJH3j/H+q/noSD5KJhz5pf9j65fKfrWcYD/bOUpmoO/SnQhWTRZtXMPXeglWi1J1HCQweMA7AIQZvji49T/sz5kLKU5dQpRpbqUqEHiti4C/cOHYxnx+Af+BBIRM8GbwlV1rYye67So/S6jrGbdiCZL1fPZBruvDWmj9+mHUfIdHAoGAaqKjTBiNI/eAFSqkmHTUggpL4S0GP4bfR2gPvbKRsTc1ALrMCl7oTczWm5sRKHiaBAes6CyB2yTQsSSq1isLn5RefSW/1pOtCqXojIrfNtCuW+CAY0tOKa8I73KkMqgxRx/4ZM+veUtq4wiC+H2P6t21MWm1xqWee6vUoL6q5GA=+EWcMY6HRtgAWpz0JAXYOTVDGkNhSeGxScOCnzPzirhC0+YBcTmIrqxpcgqwdGADK/9EbVBUg+3BqBMPbbAjxE2pXo0dc6IfoBIkVVPnZtuS1VnTVoBhvpu7HO/h6VaMCn/fw/Fin+Hr0Zg3IU5zIgjEw4GW9oaHAF/uW4MNRdUhRo+esdQ2sSqAKf4DpNOx2r+5D5kkWTGUk+3z1ZHxyp3PBTEFQvKiC/r81AgMBAAECggEADYtCOSYVRl3ip4lJ9EIMArrftgQGyYv/1WhXpramnNqPgUATJgH2YjMTaIiP8v1uOodgS2RH1AY2P5BTbzcpGZ03QM6rte71CiAoOMIXxCzXpFLlmRNOLmDkL4mjEr/EzwkHXVRJeNDS6ETk6/4Vu7b8wAejnNxW5+IdtwnLugilH3hX2dm9fJ1pNMR2rZ1f7G1y2A44IlSs52BwtvVmGbYPfCqV+rNKYYNHOuwzIAB+aEKSs4rACHoAQZuuzUAFYlcAOoBuYLADKPNjLM8QtrUl8KNF7DWGeQVq+8LhDKXOE82Kj/iWoSIq7iuQxg/OHuHezIS6Vdn8XX3gjFBdVQKBgQD98NsiuMOxqPmNimx7gHjm8IcSPbWONd9CpZaLgD0Z6IdnMe5Ub62nV9cR8QsQZedp3mbF28zrgDM408mbR6ymOzIyR5ZulY/MM5WN3ykviQfvOQfKVB7nQiae0qCwksSfCUp3tcDz63m36PbEutJDpN7UtO2hPPULMhxJwPNBowKBgQCBoQoMdTGT/iHLtQ4IFR6gcFBHTjIOGj6TpBjog0rJzrYqLXkyEJSNFNivVit/lScTRb9b57A9wXHo2IY7RydzQcJFZk5pYAA91B+UcjlktKlyTzHgECgm2jvj4ohE2wzG5B7hlH/sE+9dWzpL/Df6Ifr3yz4S8yPEGgTVM5D5RwKBgHnQpm47j7STyFJ7ipqcAGNYEpNPgt5bPWQtJtB4jdb10q5QJ3HduMHDw2nZ5WLSX3ZLt6VGUUDPWMLz5YxVj56WWvf/m0wk9rwQKrfJy2oAJAw/oVsPi1uIQhqCE49geG2J+Wz/hTX9SYBNgsLQk7jbSAFlYXCtWycpvKQsqQRdAoGABBoukTpcOUy/kQzHxuW0NGuZfsgwuVWHSXDWAPW2tjA+f5M01KrFqLlSKipV8hGsTOXWKnhokWu/yRihFtsnmnQoS73zGU6ac1p1LWN4AYHeIunysB6Sxrf9UTboDRXJj7nEaGskqTnwR0oNzD3NH7NXl+PssPjU1u3j3UNmgR0CgYAA/I5ru5qNHCBfv4iHrgfFNfYF0GuxIRVyPe4ANXPeLMLskPJBODZyCq6uXxflAQ3oJHABIkbT7XpibxP7Bd8mTQ2G09ZuiEcpWWLg75XLYgPTqzXZ+7/enMpmHrAMfUy1jfgOtMuYHMonCSja/FdHt/vqJi7DO8g1uLfz1zC0KA==";
    //支付宝公钥
    public static final String ALI_PAYPUBLIC_KEY ="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkCv+PGl/oVtLwNae4G0M4yVS0vNkrmQ9exltRSsRi0hPM+873vDXjLVWBqC+cS9RlrXv6b+b4RRKYjAwozHSTT1x1qD9hh3O8UKNpq/kJTkczly/TXeize3XOD+VjWPNgGiPkpbqJ2UanW3rBFcDFR1CLCJNp4s6jm+6oLB0ADDHTnNHiQdibSDoH3ktph+M+OmrcoW03/Q1A8za8woSz9FK19Wdv/QfsGOup3QLdA6TNRWegWTLZnj4UyfKhWCTcw/JoLp4NCLOU5Xciuk2Bcwowi+zU48vxivBjB1XhPR8AZwbYWZLoq+dWmwT514dwmdO7gfVc3XU9tTThdLNHQIDAQAB";


    @PostMapping("/{orderId}")
    @ResponseBody
    public Result pay(@PathVariable("orderId") String orderId) throws AlipayApiException {
        //01 查询订单信息
        Order order = is.getById(orderId);
        //1、创建AlipayClient
        AlipayClient alipayClient = new DefaultAlipayClient(
                saveURL,        // 支付宝网关（固定）。
                appId,          //APPID 即创建应用后生成。
                privateKey,     //私钥
                "json",
                "utf-8",
                ALI_PAYPUBLIC_KEY,//公钥
                "RSA2");        //加密方式
        //2. 创建 Request并设置Request参数
        AlipayTradePagePayRequest request = new AlipayTradePagePayRequest();
        //异步接收地址，仅支持http/https，公网可访问
        request.setNotifyUrl("http://jv34nm.natappfree.cc/pay/notifyUrl");
        //同步跳转地址，仅支持http/https
        request.setReturnUrl("http://jv34nm.natappfree.cc/pay/returnUrl");
        /******必传参数******/
        JSONObject bizContent = new JSONObject();
        //商户订单号，商家自定义，保持唯一性
        bizContent.put("out_trade_no", orderId);
        //支付金额，最小值0.01元
        bizContent.put("total_amount", order.getPrice());
        //订单标题，不可使用特殊符号
        bizContent.put("subject", order.getCourseId());
        //电脑网站支付场景固定传值FAST_INSTANT_TRADE_PAY
        bizContent.put("product_code", "FAST_INSTANT_TRADE_PAY");
        request.setBizContent(bizContent.toString());
        AlipayTradePagePayResponse response = alipayClient.pageExecute(request);
        String form = response.getBody();
        System.out.println(form);
        if (response.isSuccess()) {
            return toDataResult(form) ;
        } else {
            return toDataResult(form) ;
        }
    }


    @PostMapping("/notifyUrl")
    @ResponseBody
    public String notifyUrl(HttpServletRequest request) {
        System.out.println("回调通知....==============>");

        //获取支付宝POST过来反馈信息
        Map<String, String> params = new HashMap<>();
        Map requestParams = request.getParameterMap();
        for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext(); ) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i]
                        : valueStr + values[i] + ",";
            }

            //乱码解决，这段代码在出现乱码时使用。
            //valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
            params.put(name, valueStr);
        }
        //切记alipaypublickey是支付宝的公钥，请去open.alipay.com对应应用下查看。
        //boolean AlipaySignature.rsaCheckV1(Map<String, String> params, String publicKey, String charset, String sign_type)
        try {

            String sign = params.get("sign");
            String content = AlipaySignature.getSignCheckContentV1(params);
            boolean checkSignature = AlipaySignature.rsa256CheckContent(content, sign, ALI_PAYPUBLIC_KEY, "UTF-8"); // 验证签名

            //如果验签失败
            if (!checkSignature) {
                return "fail";
            }
            //验签成功后
            //商户的业务逻辑  比如修改订单号
            System.out.println("修改订单成功");

            //商户订单号
            String outTradeNo = params.get("out_trade_no");
            System.out.println("outTradeNo = " + outTradeNo);

            //支付宝交易凭证号
            String alipayTradeNo = params.get("trade_no");
            System.out.println("alipayTradeNo = " + alipayTradeNo);

            // 查询订单
            QueryWrapper<Order> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("id", outTradeNo);   //通过订单编号 查询
            Order order = is.getOne(queryWrapper);

            //修改订单
            if (order != null) {
                order.setTradeNo(alipayTradeNo);  //交易号
                order.setPayTime(new Date());     //付款时间
                order.setStatus(2);               //付款状态
                is.updateById(order);   //更新订单
                QueryWrapper<Course> qc = new QueryWrapper<>();
                qc.eq("id",order.getCourseId());
                Course newCourse = ics.getById(order.getCourseId());
                newCourse.setBuyCount(newCourse.getBuyCount()+1);
                ics.update(newCourse,qc);
            }

        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return "success";
    }

    /**
     * 同步操作  告诉支付跳转到 那个页面 (前端页面)
     *
     * @return
     */
    @GetMapping("/returnUrl")
    public String returnUrl() {
        /**
         * 这里不写具体的业务逻辑只做一个页面的跳转
         */
        return "redirect:http://localhost:8080/";
    }
}
