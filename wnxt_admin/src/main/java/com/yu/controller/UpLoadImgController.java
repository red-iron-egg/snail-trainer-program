package com.yu.controller;
import com.yu.config.MinioConfig;
import com.yu.config.Result;

import com.yu.util.Contents;
import com.yu.util.MinioClientUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.UUID;

@RestController
@Api(tags = "图片上传")
public class UpLoadImgController {
    @Autowired
    private MinioClientUtil minioClientUtil;
    @Autowired
    private RedisTemplate redisTemplate;
    @Resource
    private MinioConfig minioConfig;


    @PostMapping("/img")
    public Result uploadMinio(@RequestPart MultipartFile file)  {
        //获取文件名
        String fileName = file.getOriginalFilename();
        String uuid = UUID.randomUUID().toString();
        //String的方法.substring截取.lastIndexOf返回指定字符(包括字符串)右边的值
        String lost = fileName.substring(fileName.lastIndexOf("."));
        String lostFile =  uuid + lost;

        boolean flag = false;
        try {
            flag = minioClientUtil.putObject(lostFile, file.getInputStream());
        } catch (Exception e) {
            e.printStackTrace();
        }

        //传入rides
        redisTemplate.opsForSet().add(Contents.ALL_IMAGE,lostFile);
        String endpoint = minioConfig.getEndpoint();
        String img="http://"+endpoint+":9000/deposits/"+lostFile;
        return flag ? new Result(200,"大成功!",img):new Result(200,"大失败~",null);
    }
    @ApiOperation("测试传输")
    @GetMapping("/t")
    public String getHello(){
        return "HELLO WORLD";
    }




}