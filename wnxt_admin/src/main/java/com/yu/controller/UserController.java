package com.yu.controller;
import com.yu.DTO.UserVO;
import com.yu.config.Result;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.ognl.Token;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/user")
@Slf4j
@CrossOrigin
public class UserController extends BaseController{
    @ApiOperation("模拟登录")
    @PostMapping("/login")
    public Result login(@RequestBody Map<String, String> params){
        String username = params.get("username");
        String password = params.get("password");
        log.debug("模拟登录: username = " + username + ", password = " + password);

        Map<String, Object> data = new HashMap<>();
        data.put("token", username);
        return toDataResult(data);
    }

    @ApiOperation("模拟获取用户信息")
    @GetMapping("/info")
    public Result info(String token){
        log.debug("模拟获取用户信息: token = " + token);
        Map<String, Object> data = new HashMap<>();
        data.put("name", token);
        data.put("avatar", "https://img1.baidu.com/it/u=2838100141,2488760005&fm=253&app=138" +
                "&size=w931&n=0&f=JPEG&fmt=auto?sec=1663520400&t=7091611ba73832e3566f237c4e9a09ed");
        data.put("roles", new String[]{"admin", "teacher", "student"});

        return toDataResult(data);
    }
    @ApiOperation("模拟顾客登录")
    @PostMapping("/login2")
    public Result login2(@RequestBody UserVO uo){
        String Token = uo.getMobile();
        return toDataResult(   Token );
    }

}
