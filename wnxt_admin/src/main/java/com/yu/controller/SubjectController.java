package com.yu.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.yu.config.Result;
import com.yu.pojo.Subject;
import com.yu.service.ISubjectService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 课程科目 前端控制器
 * </p>
 *
 * @author 陈浩南
 * @since 2023-06-20
 */
@RestController
@RequestMapping("/SJ")
@Api(tags = "课程菜单控制器")
public class SubjectController extends BaseController {
    @Autowired
    private ISubjectService subjectService;


    @ApiOperation("获取一层菜单")
    @GetMapping("/{id}")
    public Result getOneLevel(@PathVariable String id) {
        List<Subject> subjectList =  subjectService.listByParentId(id);
        return toDataResult(subjectList);
    }

    @ApiOperation("获取一层菜单")
    @GetMapping("/all")
    public Result getAllTwo() {
        LambdaQueryWrapper<Subject> lw = Wrappers.lambdaQuery();
        List<Subject> list = subjectService.list(lw.gt(Subject::getParentId, 0));
        return toDataResult(list);
    }

}
