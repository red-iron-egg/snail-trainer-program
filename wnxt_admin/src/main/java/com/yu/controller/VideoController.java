package com.yu.controller;

import com.yu.config.Result;
import com.yu.pojo.Video;
import com.yu.service.IVideoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 课程视频 前端控制器
 * </p>
 *
 * @author 陈浩南
 * @since 2023-06-24
 */
@RestController
@RequestMapping("/video")
@Api(tags = "视频控制器")
public class VideoController extends BaseController {
    @Autowired
    private IVideoService iVideoService;

    @GetMapping("/{id}")
    @ApiOperation("id取视频")
    public Result getVideoById(@PathVariable String id){
        Video byId = iVideoService.getById(id);
        return toDataResult(byId);
    }
    @DeleteMapping("/{id}")
    @ApiOperation("id删除")
    public Result deltById(@PathVariable String id){
        boolean b = iVideoService.removeById(id);
        return toResult(b);
    }
    @PostMapping
    @ApiOperation("添加或修改")
    public Result addOrUpdate(@RequestBody Video video){
        boolean b = iVideoService.saveOrUpdate(video);
        return toResult(b);
    }



}
