package com.yu.controller;
import com.yu.DTO.QueryTrainerVO;
import com.yu.config.PageData;
import com.yu.config.Result;
import com.yu.pojo.Trainer;
import com.yu.service.TrainerService;
import com.yu.util.Contents;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 培训师 前端控制器
 * </p>
 *
 * @author 陈浩南
 * @since 2023-06-13
 */
@RestController
@RequestMapping("/WT")
@Api(tags = "培训师管理")
public class TrainerController extends BaseController{
    @Autowired
    private TrainerService trainerService;
    @Autowired
    private RedisTemplate redisTemplate;
    @PostMapping
    @ApiOperation("添加对象的方法")
    public Result add(@RequestBody Trainer trainer){
        boolean save = trainerService.save(trainer);
        if (save){
            String avatar = trainer.getAvatar();
            String substring = avatar.substring(avatar.lastIndexOf("/") + 1);
            redisTemplate.opsForSet().add(Contents.DB_IMAGE,substring);
        }
        return toResult(save);
    }

    @ApiOperation("修改的方法")
    @PostMapping("/update")
    public Result update(@RequestBody Trainer trainer){
        return toResult(trainerService.updateById(trainer));
    }

    @DeleteMapping("/{id}")
    @ApiOperation("id删除的方法")
    public Result deltForId(@PathVariable("id") String id){
        return toResult(trainerService.removeById(id));
    }

    @ApiOperation("分页+搜索")
    @PostMapping("/list/search/{currentPage}/{pageSize}")
    public Result listSearch(@PathVariable Integer currentPage,
                             @PathVariable Integer pageSize,
                             @RequestBody QueryTrainerVO queryTrainerVO){
        PageData pageData = trainerService.listSearch(currentPage, pageSize, queryTrainerVO);
       // Result r=new Result(200,"大成功");
        return toDataResult(pageData);
    }
    @ApiOperation("id查询个人信息")
    @GetMapping("/{id}")
    public Result getTById(@PathVariable String id){
       return toDataResult(trainerService.getById(id));
    }
    @ApiOperation("ids批量删除")
    @DeleteMapping
    public Result deleteIds(@RequestBody List<String> ids){
        System.out.println(ids);
       return toResult(trainerService.removeBatchByIds(ids));
    }
    @ApiOperation("查全部")
    @GetMapping("/all")
    public Result getAll(){
        return toDataResult(trainerService.list());
    }




}
