# 基础镜像
FROM openjdk:8
# 工作目录
WORKDIR ./
# 添加到镜像中
ADD ./wnxt_admin/target/wnxt_admin-1.0-SNAPSHOT.jar ./
# 端口
EXPOSE 8083
# 运行命令
ENTRYPOINT ["java","-jar"]
CMD ["wnxt_admin-1.0-SNAPSHOT.jar"]