package com.yu;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.rules.DateType;

import java.util.Collections;

public class App {

    public static void main(String[] args) {

        String path = "F:\\wnxt_parent\\wnxt-generator\\src\\main\\";

        FastAutoGenerator.create("jdbc:mysql://localhost:3306/wnxt",
                        "root", "123456")
                .globalConfig(builder -> {
                    builder.author("陈浩南") // 设置作者
                            .enableSwagger() // 开启 swagger 模式
                            .dateType(DateType.ONLY_DATE) //时间设置
                            .outputDir(path + "java"); // 指定输出目录
                })

                .packageConfig(builder -> {
                    builder.parent("com.yu") // 设置父包名
                            .moduleName(null) // 设置父包模块名
                            .entity("pojo") // 设置实体类的包名
                            .pathInfo(Collections.singletonMap(OutputFile.xml, path + "resources\\mapper")); // 设置mapperXml生成路径
                })
                .strategyConfig(builder -> {
                    builder.addInclude("wnxt_order") // 设置需要生成的表名
                            .addTablePrefix("wnxt_"); // 设置过滤表前缀
                    builder.controllerBuilder().enableRestStyle();// 设置restController
                    builder.entityBuilder().enableLombok();
                })
                .execute();
    }
}


