package com.yu.pojo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class CourseQueryVo {
    // id
    @ApiModelProperty("课程ID")
    private String id;
    @ApiModelProperty("课程封面")
    private String cover;
    @ApiModelProperty("课程标题")
    private String title;
    @ApiModelProperty("报名数量")
    private long buyCount;
    @ApiModelProperty("视频总量")
    private Integer videoCount;
}