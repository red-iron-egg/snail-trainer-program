package com.yu.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 课程科目
 * </p>
 *
 * @author 陈浩南
 * @since 2023-06-20
 */
@Getter
@Setter
  @TableName("wnxt_subject")
@ApiModel(value = "Subject对象", description = "课程科目")
public class Subject implements Serializable {

    private static final long serialVersionUID = 1L;

      @ApiModelProperty("课程类别ID")
      private String id;

      @ApiModelProperty("类别名称")
      private String title;

      @ApiModelProperty("父ID")
      private String parentId;

      @ApiModelProperty("排序字段")
      private Integer sort;

      @ApiModelProperty("创建时间")
      private Date createTime;

      @ApiModelProperty("更新时间")
      private Date updateTime;


}
