package com.yu.pojo;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 培训师
 * </p>
 *
 * @author 陈浩南
 * @since 2023-06-13
 */
@Getter
@Setter
@TableName("wnxt_trainer")
@ApiModel(value = "Trainer对象", description = "培训师")
public class Trainer implements Serializable {

    private static final long serialVersionUID = 1L;

      @ApiModelProperty("培训师ID")
      @TableId(value = "id",type = IdType.ASSIGN_ID)//指定id生成策略为雪花算法id
        private String id;

      @ApiModelProperty("培训师姓名")
      private String name;

      @ApiModelProperty("培训师简介")
      private String intro;

      @ApiModelProperty("培训师资历,一句话说明培训师")
      private String career;

      @ApiModelProperty("头衔 1高级培训师 2首席培训师")
      private Integer level;

      @ApiModelProperty("培训师头像")
      private String avatar;

      @ApiModelProperty("排序")
      private Integer sort;

      @ApiModelProperty("逻辑删除 1（true）已删除， 0（false）未删除")
      @TableLogic//逻辑删除字段
      private Integer isDeleted;

      @ApiModelProperty("创建时间")
      @TableField(fill = FieldFill.INSERT)
      private Date createTime;

      @ApiModelProperty("更新时间")
      @TableField( fill = FieldFill.INSERT_UPDATE)
      private Date updateTime;


}
