package com.yu.pojo;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author 陈浩南
 * @since 2023-06-28
 */
@Getter
@Setter
  @TableName("wnxt_order")
@ApiModel(value = "Order对象", description = "订单")
public class Order implements Serializable {

    private static final long serialVersionUID = 1L;

      @ApiModelProperty("主键")
      @TableId(type= IdType.ASSIGN_ID)
        private String id;

      @ApiModelProperty("购买课程id")
      private String courseId;

      @ApiModelProperty("支付宝交易号")
      private String tradeNo;

      @ApiModelProperty("下单用户")
      private Long userId;

      @ApiModelProperty("订单金额")
      private BigDecimal price;

      @ApiModelProperty("付款时间")
      private Date payTime;

      @ApiModelProperty("订单状态(1：待支付 2：已支付 3：进行中 4：已取消 5：已完成 6.退款中 7. 已退款)")
      private Integer status;

      @ApiModelProperty("手机号")
      private String mobile;

      @ApiModelProperty("下单时间")
      @TableField(fill = FieldFill.INSERT)
      private Date createTime;

      @ApiModelProperty("修改时间")
      @TableField(fill = FieldFill.INSERT_UPDATE)
      private Date updateTime;


}
