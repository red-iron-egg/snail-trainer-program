package com.yu.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author 陈浩南
 * @since 2023-06-21
 */
@Getter
@Setter
  @TableName("sys_menu")
@ApiModel(value = "Menu对象", description = "")
public class Menu implements Serializable {

    private static final long serialVersionUID = 1L;

      @ApiModelProperty("编号")
        @TableId(value = "id", type = IdType.AUTO)
      private Integer id;

      @ApiModelProperty("所属上级")
      private Integer parentId;

      @ApiModelProperty("权限名称")
      private String pname;

      @ApiModelProperty("菜单路径")
      private String url;

      @ApiModelProperty("权限标识")
      private String code;

      @ApiModelProperty("1表示一级菜单;2为二级菜单")
      private Integer level;

      @ApiModelProperty("排序")
      private Integer sort;

      @ApiModelProperty("菜单图标")
      private String icon;


}
