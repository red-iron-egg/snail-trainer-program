package com.yu.config;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data@AllArgsConstructor@NoArgsConstructor@ApiModel(value = "分页响应对象",description = "分页响应结果")
public class PageData {
    @ApiModelProperty("总记录条数")
    private Long total;
    @ApiModelProperty("分页数据")
    private Object data;
}
