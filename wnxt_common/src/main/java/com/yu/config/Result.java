package com.yu.config;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data@ApiModel(value = "Result对象",description = "封装响应结果")
public class Result {
    @ApiModelProperty("响应状态码")
    private Integer code;
    @ApiModelProperty("响应的提示信息")
    private String message;
    @ApiModelProperty("响应数据")
    private Object data;

    public Result(Integer code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public Result(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
